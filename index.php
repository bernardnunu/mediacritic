<?php require("php/main.php"); ?>

<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" data-theme="light">
<head>
    <meta charset="UTF-8">
    <link rel="canonical" href="www.mediacritic.fr">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="MediaCritic, site de reférence en critiques de films, séries et jeux vidéos, éléments populaires.">
    <meta property="og:title" content="MediaCritic - Critiques de films, séries et jeux vidéos">
    <meta property="og:description" content="MediaCritic, site de reférence en critiques de films, séries et jeux vidéos, éléments populaires.">
    <meta property="og:url" content="https://www.mediacritic.fr">
    <meta property="og:locale" content="fr_FR">
    <meta property="og:image" content="https://www.mediacritic.fr/favicon.ico">
    <meta property="og:type" content="website">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/css/globaux.css">
    <title>MediaCritic - Critiques de films, séries et jeux vidéos</title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155163165-1"></script>
    <script src="/js/google.js"></script>
</head>
<body>

<div id="grid">

    <?php require("php/header/header.php"); ?>

    <main id="main">

        <div id="partieun">
            <div class="alert"><p class="messagealert">Liste des <span><?php echo $_SESSION['texte']; ?></span> les plus populaires.</p></div>
            <form class="filter" method="post">
                <p class="texteFilter">Sélectionnez la catégorie à afficher</p>
                <div id="coche">
                    <input type="submit" name="all" value="Tout" class="coche">
                    <input type="submit" name="film" value="Film" class="coche">
                    <input type="submit" name="serie" value="Série" class="coche">
                    <input type="submit" name="jv" value="Jeu vidéo" class="coche">
                </div>
            </form>
        </div>

        <?php

            $nbNotes = 0;
            $moyenne = 0.00;
            $items = array();
            $listItems = array();
            $sql = "SELECT items.iditems, items.titre FROM items 
            INNER JOIN rates ON items.iditems = rates.iditems INNER JOIN categorie ON items.idcategorie = categorie.idcategorie ".$_SESSION['req'].";";
            foreach ($bdd->query($sql) as $item){
                if (! in_array($item['iditems'], $listItems)) {
                    $nbNotes = 0;
                    $moyenne = 0.00;
                    $sommeNotes = 0;
                    $sql2 = "SELECT rates FROM rates WHERE iditems = '" . $item['iditems'] . "';";
                    foreach ($bdd->query($sql2) as $rate){
                        $sommeNotes = $sommeNotes + intval($rate['rates']);
                        $nbNotes++;
                    }
                    if($nbNotes!=0){
                        $moyenne = $sommeNotes / $nbNotes;
                    }else{
                        $moyenne = 0;
                    }
                    array_push($items, array($item['iditems'], $moyenne, $item['titre'], $nbNotes));
                    array_push($listItems, $item['iditems']);
                }
            }

            $moyenneKeys = array_column($items, 1);
            $nbNoteKeys = array_column($items, 3);
            $titreKeys = array_column($items, 2);
            array_multisort($moyenneKeys, SORT_DESC, $nbNoteKeys, SORT_DESC, $titreKeys, SORT_ASC, $items);
            
            $itemsTotals2 = count($items);
            $pagesTotales2 = ceil($itemsTotals2/$itemsParPage);
            $startingPosition = ($pageCourante - 1) * $itemsParPage;
            $compteur = 0;
            $cpt = 1;

        ?>

        <div class="pagination">

            <div class="content">

                <?php

                if($pageCourante > 2){
                    echo "<a class='page' href='index.php?page=1'>1</a><span> ... </span>";
                }
                for($i = $pageCourante-1; $i <= $pageCourante+1; ++$i){
                    if($i == $pageCourante){
                        if($itemsTotals2 > 0){
                            echo "<a id='pageCourante' class='page'>$i</a>";
                        }else{
                            echo "<a id='pageCourante' class='page'>Pas de page</a>";
                        }
                    }else{
                        if($itemsTotals2 > 0){
                            if($i > 0 AND $i < $pagesTotales2+2 AND $i > $pageCourante){
                                if($pageCourante < $pagesTotales2){
                                    echo "<a class='page' href='index.php?page=".$i."'><i class='fas fa-forward'></i></a>";
                                }
                            }else{
                                if($pageCourante != 1 AND $i > 0){
                                    echo "<a class='page' href='index.php?page=".$i."'><i class='fas fa-backward'></i></a>";
                                }
                            }
                        }
                    }
                }
                if($pageCourante < $pagesTotales2-1){
                    echo "<span> ... </span><a class='page' href='index.php?page=".$pagesTotales2."'>".$pagesTotales2."</a>";
                }

                ?>

            </div>

        </div>
        
        <div id="items">

        <?php

            $line = array();
            $_SESSION['tabId'] = array();
            for ($i=$startingPosition; $i<$itemsTotals2; ++$i) {
                $line = $items[$i];
                $sql = $bdd->query("SELECT items.iditems, items.titre, items.titreURL, categorie.categorie, categorie.idcategorie, images.image FROM items 
                INNER JOIN categorie ON items.idcategorie = categorie.idcategorie 
                INNER JOIN itemsimages ON itemsimages.iditems = items.iditems 
                INNER JOIN images ON images.id = itemsimages.idimages AND images.deleted = false
                WHERE items.iditems = ".$line[0].";");
                foreach ($sql as $item){
                    $totalNotes = 0;
                    $req = $bdd->query("SELECT rates FROM rates 
                    WHERE iditems = ".$item['iditems'].";");
                    $nbNotes = $req->rowCount();
                    if($nbNotes > 1){$s = "s";}else{$s = "";}
                    foreach ($req as $note) {
                        $totalNotes = $totalNotes + intval($note['rates']);
                    }
                    $moyenne = $totalNotes / $nbNotes;
                    $moyennearrondie = number_format($moyenne,2);
                    if($nbNotes === 0){
                        $moyennearrondie = '&#8709;';
                    }
                    echo "<div class='block'>";
                    echo "<p class='category'>";  
                    if($cpt <= 3){$tabId = array_push($_SESSION['tabId'], $item['iditems']); echo "<img class='top' src='/favicon.ico' alt='Image d une étoile' title='Top ".strtolower($item['categorie'])." du moment'>";} 
                    echo "<span>".$item['categorie']."</span>";
                    echo "</p>";
                    if($connected == true && $god == true){echo "<a class='modifier' href='/mc/page-modifier.php?iditem=".$item['iditems']."'><input type='submit' value='Modifier'></a>";}
                    if($item['idcategorie'] == 1){echo "<a class='image' href='/films/".$item['titreURL']."/".$item['iditems']."'><img src='" . $item['image'] . "' alt='Image de ".str_replace("'", " ", $item['titre'])."'></a>";}
                    if($item['idcategorie'] == 2){echo "<a class='image' href='/series/".$item['titreURL']."/".$item['iditems']."'><img src='" . $item['image'] . "' alt='Image de ".str_replace("'", " ", $item['titre'])."'></a>";}
                    if($item['idcategorie'] == 3){echo "<a class='image' href='/jv/".$item['titreURL']."/".$item['iditems']."'><img src='" . $item['image'] . "' alt='Image de ".str_replace("'", " ", $item['titre'])."'></a>";}                    
                    echo "<h1 class='title_item'>" . $item['titre'] . "</h1>";
                    $real = $bdd->prepare("SELECT realisateur.realisateur FROM linkrealisateur 
                    INNER JOIN realisateur ON linkrealisateur.idRealisateur = realisateur.idrealisateur 
                    WHERE linkrealisateur.idItems = ".$item['iditems'].";");
                    $real->execute();
                    $nRows = $real->rowCount();
                    $listeReal = $real->fetch();
                    if($nRows > 1){
                        echo "<h3 class='realisateur'>" . $listeReal['realisateur'] . " ...</h3>";
                    }else{
                        echo "<h3 class='realisateur'>" . $listeReal['realisateur'] . "</h3>";
                    }
                    if($connected == true){
                        $req = $bdd->prepare("SELECT idrates FROM rates 
                        WHERE idusers = ? AND iditems = ?;");
                        $req->execute(array($userID, $item['iditems']));
                        $reqtrue = $req->fetch();
                        if($reqtrue == false){
                        echo ' <form class="stars" action="/php/traitement.php" method="post">
                                    <input name="itemid" type="hidden" value="' . $item['iditems'] . '">';
                        echo '      <input type="submit" class="star" name="1" value="">
                                    <input type="submit" class="star" name="2" value="">
                                    <input type="submit" class="star" name="3" value="">
                                    <input type="submit" class="star" name="4" value="">
                                    <input type="submit" class="star" name="5" value="">
                                </form>';
                        echo "<div class='infos'>
                                <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                                </div>";
                        }else{
                            $knowrate = $bdd->prepare("SELECT rates FROM rates WHERE idusers = ".$userID." AND iditems=".$item['iditems']."");
                            $knowrate->execute();
                            $infoRate = $knowrate->fetch();
                            echo "<div class='infos'>
                                    <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                                    <p class='ok'>Votre note : ".intval($infoRate['rates'])."/5</p>
                                </div>";
                        }
                    }else{
                        echo "<p class='noconnect'>Connectez-vous <br>pour noter</p>";
                        echo "<div class='infos'>
                                <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                                </div>";
                    }
                    echo "</div>";
                }
                $cpt++;
                $compteur++;
                if($compteur > $itemsParPage-1){
                    break;
                }
            }

        ?>

        </div>

        <div class="pagination">

            <div class="content">

                <?php

                if($pageCourante > 2){
                    echo "<a class='page' href='index.php?page=1'>1</a><span> ... </span>";
                }
                for($i = $pageCourante-1; $i <= $pageCourante+1; ++$i){
                    if($i == $pageCourante){
                        if($itemsTotals2 > 0){
                            echo "<a id='pageCourante' class='page'>$i</a>";
                        }else{
                            echo "<a id='pageCourante' class='page'>Pas de page</a>";
                        }
                    }else{
                        if($itemsTotals2 > 0){
                            if($i > 0 AND $i < $pagesTotales2+2 AND $i > $pageCourante){
                                if($pageCourante < $pagesTotales2){
                                    echo "<a class='page' href='index.php?page=".$i."'><i class='fas fa-forward'></i></a>";
                                }
                            }else{
                                if($pageCourante != 1 AND $i > 0){
                                    echo "<a class='page' href='index.php?page=".$i."'><i class='fas fa-backward'></i></a>";
                                }
                            }
                        }
                    }
                }
                if($pageCourante < $pagesTotales2-1){
                    echo "<span> ... </span><a class='page' href='index.php?page=".$pagesTotales2."'>".$pagesTotales2."</a>";
                }

                ?>
                
            </div>

        </div>

    </main>

    <?php require("php/footer/footer.php"); ?>

</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
</html>