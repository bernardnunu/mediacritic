<?php

require("bdd.php");

require("menus.php");

// Redirection

if($connected == false){
    header("Location: /");
    exit();
}

// Modifier profil

if(isset($_POST['changer'])){
    if(!empty($_POST['pseudo']) AND !empty($_POST['email']) AND !empty($_POST['mdp'])){
        if(preg_match("/^[a-z0-9]+[a-z0-9\-_]+[a-z0-9]{1}$/i", $_POST['pseudo'])){
            if(preg_match("/^[a-z0-9]{1}[a-z0-9\-_]+[a-z0-9]{1}@[a-z0-9]+\.[a-z]{1,15}$/i", $_POST['email'])){
                if(preg_match("/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])[-+!$@%_\w]{8,20}$/", $_POST['mdp'])){

                    $mdp_crypted = hash('sha256', $_POST['mdp']);
                    $oldmdp_crypted = hash('sha256', $_POST['oldmdp']);
                    $pseudo = trim($_POST['pseudo']);
                    $email = trim($_POST['email']);

                    if(strlen($_POST['pseudo']) <= 20 AND strlen($_POST['pseudo']) >= 4){
                        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                            if(strlen($_POST['mdp']) >= 8 AND strlen($_POST['mdp']) <= 20){
                                
                                $reqpseudo = $bdd->prepare("SELECT * FROM users WHERE pseudo = ? AND id != $userID");
                                $reqpseudo->execute(array($pseudo));
                                $verpseudo = $reqpseudo->fetch();

                                if($verpseudo == false){

                                    $reqemail = $bdd->prepare("SELECT * FROM users WHERE email = ? AND id != $userID");
                                    $reqemail->execute(array($email));
                                    $veremail = $reqemail->fetch();

                                    if($veremail == false){

                                        $reqmdp = $bdd->prepare("SELECT * FROM users WHERE motdepasse = ? AND id = $userID");
                                        $reqmdp->execute(array($oldmdp_crypted));
                                        $vermdp = $reqmdp->fetch();

                                        if($vermdp == true){
                                            
                                            $reqchange = $bdd->prepare("SELECT * FROM users WHERE email = ? AND id = $userID");
                                            $reqchange->execute(array($email));
                                            $verchange = $reqchange->fetch();

                                            if($verchange == false){
                                                $change = $bdd->prepare("UPDATE users SET validated = 0 WHERE id = $userID");
                                                if($change->execute()){

                                                    $headers = array(
                                                        'From' => 'webmaster',
                                                        'Reply-To' => 'webmaster@mediacritic.fr',
                                                        'X-Mailer' => 'PHP/' . phpversion()
                                                    );

                                                    $envoiemail = mail($email, "Validation de votre nouvelle adresse", "Bonjour ".$pseudo." !\n\nVous avez récemment changer votre adresse mail sur mediacritic.fr.\nVeuillez la valider grâce à ce lien -> https://mediacritic.fr/mc/page-validation.php?uuid=".$userinfo['uuid_validation']."\n\nÀ Bientôt !\n\nNOTE : Ceci est un mail provisoire (design), il sera modifié d'ici quelques semaines.", $headers);
                                                    $message2 = "Veuillez confirmer votre nouvelle adresse mail pour vous reconnecter.";
                                                    
                                                }else{
                                                    $message = "Une erreur s'est produite.";
                                                }
                                            }

                                                if($_FILES['logo']['size'] == 0){

                                                    $req = $bdd->prepare("UPDATE users SET pseudo = ?, email = ?, motdepasse = ? WHERE id = $userID;");
                                                    if(! $req->execute(array($pseudo, $email, $mdp_crypted))){
                                                        print_r($req->errorInfo());
                                                    }else{
                                                        $message1 = "Votre profil a bien été modifié.";
                                                    }

                                                }else{
                                                    
                                                    $errors= array();
                                                    $file_name = $_FILES['logo']['name'];
                                                    $file_size = $_FILES['logo']['size'];
                                                    $file_tmp = $_FILES['logo']['tmp_name'];
                                                    $file_type = $_FILES['logo']['type'];
                                                    $explode = explode('.',$file_name);
                                                    $file_ext = strtolower(end($explode));
                                                    $file_ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                                                    $extensions = array("jpeg","jpg","png");

                                                    if(in_array($file_ext,$extensions)=== false){
                                                        $errors[]="L'extention de l'image n'est pas valide (PNG, JPG, JPEG).";
                                                    }
                                                        
                                                    if($file_size > 2097152){
                                                        $errors[]= "L'image ne doit pas dépasser 2MB.";
                                                    }

                                                    $reqImage = $bdd->prepare("SELECT * FROM usersimages WHERE idUsers = $userID");
                                                    $reqImage->execute();
                                                    $verImage = $reqImage->fetch();

                                                    if($verImage == true){

                                                        if(empty($errors)==true){
                                                            $file_uniqueid = md5(uniqid() . $file_name . rand(0,100));
                                                            $chemin_image = "../logos/".$file_uniqueid.".$file_ext";
                                                            $image = move_uploaded_file($file_tmp,$chemin_image);
                                                                if($image == true){
                                                                    try{
                                                                        $bdd->beginTransaction();
                                                                        date_default_timezone_set('Europe/Paris');
                                                                        $date = date('Y-m-d H:i:s');
                                                                        $bddGetImage = $bdd->prepare("SELECT * FROM usersimages WHERE idUsers = ? ORDER BY date DESC LIMIT 1;");
                                                                        $bddModifyImage = $bdd->prepare("UPDATE images SET DeletedDate = ?, deleted = ? WHERE id = ?");
                                                                        $bddImage = $bdd->prepare("INSERT INTO images (image, dateAjout) VALUES (?, ?);");
                                                                        $bddUser = $bdd->prepare("UPDATE users SET (pseudo, email, motdepasse) VALUES (?, ?, ?) WHERE id = ?;");
                                                                        $bddUsersImages = $bdd->prepare("INSERT INTO usersimages (idUsers, idImages, date) VALUES (?, ?, ?);");
                                                                        //Récuperer l'ID de l'image en cours
                                                                        $bddGetImage->execute(array($userID));
                                                                        $requeteImage = $bddGetImage->fetch();
                                                                        $idImage = intval($requeteImage['idImages']);
                                                                        // Modifier le champs deleted et deleted_date de l'image en cours
                                                                        $bddModifyImage->execute(array($date, 1, $idImage));
                                                                        // Ajouter l'enregistremant dans la table images
                                                                        $bddImage->execute(array(preg_replace('/^\.\./', '', $chemin_image), $date));
                                                                        $lastIdImage = $bdd->lastInsertId();
                                                                        // Ajouter l'enregistrement dans la table ItemsImages
                                                                        $bddUsersImages->execute(array($userID, $lastIdImage, $date));
                                                                        // Ajouter l'enregistrement dans la table Items
                                                                        $bddUser->execute(array($pseudo, $email, $mdp_crypted, $userID));
                                                                        if(! $bdd->commit()){
                                                                            print_r($bdd->errorInfo());
                                                                        }else{
                                                                            $message1 = "Votre profil a été mis à jour.";
                                                                        }
                                                                    }catch( PDOExecption $e ){
                                                                        $bdd->rollback();
                                                                        $message = "Erreur: " . $e->getMessage();
                                                                    }
                                                                }else{
                                                                    $message = "Erreur lors de l'upload de l'image.";
                                                                }
                                                        }else{
                                                            $message = "Une erreur s'est produite.";
                                                            print_r($errors);
                                                        }

                                                    }else{
                                    
                                                        if(empty($errors)==true){
                                                            $file_uniqueid = md5(uniqid() . $file_name . rand(0,100));
                                                            $chemin_image = "../logos/".$file_uniqueid.".$file_ext";
                                                            $image = move_uploaded_file($file_tmp,$chemin_image);
                                                            if($image == true){
                                                                try{
                                                                    $bdd->beginTransaction();
                                                                    date_default_timezone_set('Europe/Paris');
                                                                    $date = date('Y-m-d H:i:s');
                                                                    $bddImage = $bdd->prepare("INSERT INTO images (image, dateAjout) VALUES (?, ?);");
                                                                    $bddUser = $bdd->prepare("UPDATE users SET (pseudo, email, motdepasse) VALUES (?, ?, ?);");
                                                                    $bddUsersImages = $bdd->prepare("INSERT INTO usersimages (idUsers, idImages, date) VALUES (?, ?, ?);");
                                                                    $bddImage->execute(array(preg_replace('/^\.\./', '', $chemin_image), $date));
                                                                    $lastIdImage = $bdd->lastInsertId();
                                                                    $bddUser->execute(array($pseudo, $email, $mdp_crypted));
                                                                    // $lastIdUser = $bdd->lastInsertId();
                                                                    $bddUsersImages->execute(array($userID, $lastIdImage, $date));
                                                                    $idUsersImages = $bdd->lastInsertId();
                                                                    if(! $bdd->commit()){
                                                                        print_r($bdd->errorInfo());
                                                                    }else{
                                                                        $message1 = "Votre profil a été mis à jour.";
                                                                    }
                                                                }catch( PDOExecption $e ){
                                                                    $bdd->rollback();
                                                                    $message = "Erreur: " . $e->getMessage();
                                                                }
                                                            }else{
                                                                $message = "Erreur lors de l'upload de l'image.";
                                                            }
                                                        }else{
                                                            $message = "Une erreur s'est produite.";
                                                            print_r($errors);
                                                        }

                                                    }

                                                }

                                        }else{
                                            $message = "Mot de passe actuel incorrect.";
                                        }
                                    }else{
                                        $message = "Cet email est déjà utilisé.";
                                    }
                                }else{
                                    $message = "Ce pseudo est déjà utilisé.";
                                }

                            }else{
                                $message = "Votre mot de passe doit contenir entre 8 et 20 caractères.";
                            }
                        }else{
                            $message = "Votre email n'est pas valide.";
                        }
                    }else{
                        $message = "Votre pseudo doit contenir entre 4 et 20 caractères.";
                    }
                }else{
                    $message = "Votre mot de passe doit contenir minimum une minuscule, une majuscule, un chiffre et un caractère spécial.";
                }
            }else{
                $message = "Mauvaise syntaxe pour le champs 'Email'.";
            }
        }else{
            $message = "Mauvaise syntaxe pour le champs 'Pseudo'.";
        }    
    }else{
        $message = "Veuillez renseigner tous les champs.";
    }
}
    
?>