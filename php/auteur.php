<?php

session_start();

require("bdd.php");

require("menus.php");

// Redirection

if($connected == false || $god == false){
    header("Location: /");
    exit();
}

// Traitement

if(isset($_POST['soumettre'])){
    if(empty($_POST['modifierAuteur']) && ! empty($_POST['ajouterAuteur'])){
        if(! preg_match("/[!?;*<>=()\[\]\|\/\$]/", $_POST['ajouterAuteur'])){

            $newAuteur = trim($_POST['ajouterAuteur']);

            $reqExist = $bdd->prepare("SELECT realisateur FROM realisateur WHERE realisateur = '$newAuteur'");
            $reqExist->execute();
            $exist = $reqExist->fetch();

            if($exist == false){
                
                $insert = $bdd->prepare("INSERT INTO realisateur(realisateur) VALUES ('$newAuteur');");
                
                if($insert->execute()){
                    $message1 = "L'auteur vient d'être ajouté.";
                }else{
                    print_r($insert->errorInfo());
                }

            }else{
                $message = "Cet auteur existe déjà.";
            }

        }else{
            $message = "Mauvaise syntaxe pour le champs 'Nouvel auteur...'.";
        }
    }elseif(! empty($_POST['modifierAuteur']) && empty($_POST['ajouterAuteur'])){

        if(! empty($_POST['realisateur'])){

            if(! preg_match("/[!?;*<>=()\[\]\|\/\$]/", $_POST['modifierAuteur'])){

                $explode = explode("|", $_POST['realisateur']);
                $modifyAuteur = trim($_POST['modifierAuteur']);

                $reqExist = $bdd->prepare("SELECT realisateur FROM realisateur WHERE realisateur = '$modifyAuteur' AND realisateur != '$explode[1]'");
                $reqExist->execute();
                $exist = $reqExist->fetch();

                if($exist == false){

                    $updateAuteur = $bdd->prepare("UPDATE realisateur SET realisateur = ? WHERE realisateur = ?;");

                    if($updateAuteur->execute(array($modifyAuteur, $explode[1]))){
                        $message1 = "L'auteur sélectionné a bien été modifié.";
                    }else{
                        print_r($updateAuteur->errorInfo());
                    }

                }else{
                    $message = "Cet auteur existe déjà.";
                }
                
            }else{
                $message = "Mauvaise syntaxe pour le champs 'Modifier auteur...'.";
            }

        }else{
            $message = "Veuillez séléctionner un auteur pour le modifier.";
        }

    }else{
        $message = "Veuillez remplir un champs.";
    }
}

?>