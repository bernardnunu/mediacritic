<?php

require("bdd.php");

require("menus.php");

// Affichage du commentaire

$_SESSION['idavis'] = $_GET['idavis'];
$req = $bdd->prepare("SELECT * FROM avis WHERE idavis = ".$_SESSION['idavis']."");
$req->execute();
$com = $req->fetch();

// Traitement modification avis

function verifieChamp($nom){
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

if($connected == true){
    if(isset($_POST['modifier'])){
        verifieChamp('zonetexte');
        $him = $bdd->prepare("SELECT * FROM avis WHERE idavis = ".$_SESSION['idavis']." AND idusers = $userID");
        $him->execute();
        $right = $him->fetch();
        if($right == true){
            if(!preg_match("/[;*<>=)(\[\]\|&\/\$]/", $_POST['zonetexte'])){
                if(strlen($_POST['zonetexte']) < 200){

                    $zonetexte = base64_encode(trim($_POST['zonetexte']));

                    try{
                        $bdd->beginTransaction();
                        date_default_timezone_set('Europe/Paris');
                        $date = date('Y-m-d H:i:s');
                        $bddAvis = $bdd->prepare("INSERT INTO avis (avis, dateCreation, iditems, idusers, dateModification) VALUES (?, ?, ?, ?, ?);");
                        $bddAvis->execute(array($zonetexte, $com['dateCreation'], $com['iditems'], $com['idusers'], $date));
                        if(! $bdd->commit()){
                            print_r($bdd->errorInfo());
                        }else{
                            header("Location: /mc/page-fiche.php?iditem=".$com['iditems']."");
                            exit();
                        }
                    }catch( PDOExecption $e ){
                        $bdd->rollback();
                        $message = "Erreur: " . $e->getMessage();
                    }
                }else{
                    $message = "Votre message ne doit pas dépasser 200 caractères.";
                }
            }else{
                $message = "Votre avis contient des caractère(s) interdit(s).";
            }
        }else{
            $message = "Vous n'êtes pas autorisé à modifier ce commentaire.";
        }
    }
}

?>