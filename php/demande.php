<?php

require("bdd.php");

require("menus.php");

// Vérification et envoi du mail

if(isset($_POST['envoyer'])){
    if(!empty($_POST['email'])){
        if(preg_match("/^[a-z0-9]{1}[a-z0-9\-_]+[a-z0-9]{1}@[a-z0-9]+\.[a-z]{1,15}$/i", $_POST['email'])){

            $email = trim($_POST['email']);

            $reqexist = $bdd->prepare("SELECT * FROM users WHERE email = ?");
            $reqexist->execute(array($email));
            $exist = $reqexist->fetch();

            if($exist == true){

                $uuid_renew = md5(uniqid() . $email);
                $new = $bdd->prepare("UPDATE users SET renewed = ?, uuid_renew = ? WHERE email = ?");

                if($new->execute(array(1, $uuid_renew, $email))){

                    $headers = array(
                        'From' => 'webmaster',
                        'Reply-To' => 'webmaster@mediacritic.fr',
                        'X-Mailer' => 'PHP/' . phpversion()
                    );

                    $envoiemail = mail($email, "Demande de changement de mot de passe", "Bonjour !\n\nVous avez formulé une demande de changement de mot de passe sur votre compte.\nVoici le lien que vous devez utiliser pour le modifier -> https://www.mediacritic.fr/mc/page-changement.php?uuid=$uuid_renew\n\nNOTE : Ceci est un mail provisoire (design), il sera modifié d'ici quelques semaines.", $headers);
                    if($envoiemail == true){
                        $message1 = "Un email vient d'être envoyé à l'adresse renseignée, veuillez consulter votre boîte mail."; 
                    }

                }else{
                    print_r($new->errorInfo());
                }

            }else{
                $message = "Aucun compte ne possède cette adresse mail.";
            }
        }else{
            $message = "Mauvaise syntaxe pour le champs 'Email'.";
        }
    }else{
        $message = "Veuillez renseigner un email.";
    }
}

?>