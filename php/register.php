<?php

require("bdd.php");

require("menus.php");

// Inscription

function verifieChamp($nom){
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

if(isset($_POST['inscription'])){
    verifieChamp('pseudo');
    verifieChamp('email');
    verifieChamp('email2');
    if(!empty($_POST['pseudo']) AND !empty($_POST['email']) AND !empty($_POST['email2']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2'])){

        if(preg_match("/^[a-z0-9]+[a-z0-9\-_]+[a-z0-9]{1}$/i", $_POST['pseudo'])){
            if(preg_match("/^[a-z0-9]{1}[a-z0-9\-_]+[a-z0-9]{1}@[a-z0-9]+\.[a-z]{1,15}$/i", $_POST['email'])){
                if(preg_match("/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])[-+!$@%_\w]{8,20}$/", $_POST['mdp'])){

                    if(strlen($_POST['pseudo']) <= 20 AND strlen($_POST['pseudo']) >= 4){
                        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                            if(strlen($_POST['mdp']) >= 8 AND strlen($_POST['mdp']) <= 20){
                                if($_POST['mdp'] == $_POST['mdp2']){
                                    if($_POST['email'] == $_POST['email2']){
                                        $reqemail = $bdd->prepare("SELECT * FROM users WHERE email = ?");
                                        $reqemail->execute(array($_POST['email']));
                                        $emailexist = $reqemail->fetch();
                                        if($emailexist == false){
                                            $reqpseudo = $bdd->prepare("SELECT * FROM users WHERE pseudo = ?");
                                            $reqpseudo->execute(array($_POST['pseudo']));
                                            $pseudoexist = $reqpseudo->fetch();
                                            if($pseudoexist == false){

                                                $mdp_crypted = hash('sha256', $_POST['mdp']);
                                                $pseudo = trim($_POST['pseudo']);
                                                $email = trim($_POST['email']);
                                                $uuid_validation = md5(uniqid() . $pseudo);

                                                function isValid() 
                                                {
                                                    try {

                                                        $url = 'https://www.google.com/recaptcha/api/siteverify';
                                                        $data = ['secret'   => '6LeCWdsUAAAAAMKecgTS0rg-zXLXwVCD2HRw2dkj',
                                                                'response' => $_POST['g-recaptcha-response'],
                                                                'remoteip' => $_SERVER['REMOTE_ADDR']];

                                                        $options = [
                                                            'http' => [
                                                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                                                                'method'  => 'POST',
                                                                'content' => http_build_query($data) 
                                                            ]
                                                        ];

                                                        $context  = stream_context_create($options);
                                                        $result = file_get_contents($url, false, $context);
                                                        return json_decode($result)->success;
                                                    }
                                                    catch (Exception $e) {
                                                        return null;
                                                    }
                                                }

                                                if(isValid()){

                                                    $headers = array(
                                                        'From' => 'webmaster',
                                                        'Reply-To' => 'webmaster@mediacritic.fr',
                                                        'X-Mailer' => 'PHP/' . phpversion()
                                                    );

                                                    $req = $bdd->prepare("INSERT INTO users (pseudo, email, motdepasse, uuid_validation) VALUES (?, ?, ?, ?)");
                                                    $req->execute(array($pseudo, $email, $mdp_crypted, $uuid_validation));
                                                    $envoiemail = mail($email, "Validation de votre compte", "Bonjour ".$pseudo." !\n\nVous avez formulé une demande d'inscription sur mediacritic.fr.\nVoici votre lien de validation -> https://www.mediacritic.fr/mc/page-validation.php?uuid=$uuid_validation\n\nÀ Bientôt !\n\nNOTE : Ceci est un mail provisoire (design), il sera modifié d'ici quelques semaines.", $headers);

                                                    if($envoiemail == true){
                                                        header("Location: page-prevalidation.php");
                                                        exit();
                                                    }else{
                                                        $message = "Une erreur s'est produite lors de l'envoi du mail.";
                                                    }

                                                }else{
                                                    $message = "Veuillez valider le captcha avant de vous inscrire.";
                                                }

                                            }else{
                                                $message = "Ce pseudo est déjà utilisé.";
                                            }
                                        }else{
                                            $message = "Cet email est déjà utilisé.";
                                        }
                                    }else{
                                        $message = "Vos emails ne correspondent pas.";
                                    }
                                }else{
                                    $message = "Vos mots de passe ne correspondent pas.";
                                }
                            }else{
                                $message = "Votre mot de passe doit contenir entre 8 et 20 caractères.";
                            }
                        }else{
                            $message = "Votre email n'est pas valide.";
                        }
                    }else{
                        $message = "Votre pseudo doit contenir entre 4 et 20 caractères.";
                    }
                }else{
                    $message = "Votre mot de passe doit contenir minimum une minuscule, une majuscule, un chiffre et un caractère spécial.";
                }
            }else{
                $message = "Mauvaise syntaxe pour le champs 'Email'.";
            }
        }else{
            $message = "Mauvaise syntaxe pour le champs 'Pseudo'.";
        }
    }else{
        $message = "Veuillez renseigner tous les champs.";
    }
}

?>