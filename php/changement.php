<?php

require("bdd.php");

require("menus.php");

// Obtention uuid

$_SESSION['uuid'] = $_GET['uuid'];

if(isset($_POST['changer'])){
    if(!empty($_POST['mdp']) AND !empty($_POST['mdp2'])){
        if(preg_match("/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*$@%_])[-+!$@%_\w]{8,20}$/", $_POST['mdp'])){
            if($_POST['mdp'] === $_POST['mdp2']){

                $reqrenew = $bdd->prepare("SELECT * FROM users WHERE renewed = ? AND uuid_renew = ?");
                $reqrenew->execute(array(1, $_SESSION['uuid']));
                $verrenew = $reqrenew->fetch();

                if($verrenew == true){

                    $mdp_crypted = hash('sha256', $_POST['mdp']);
                    $newmdp = $bdd->prepare("UPDATE users SET renewed = ?, motdepasse = ? WHERE uuid_renew = ?");

                    if($newmdp->execute(array(0, $mdp_crypted, $_SESSION['uuid']))){
                        $message1 = "Votre mot de passe a bien été changé.";
                    }else{
                        print_r($newmdp->errorInfo());
                    }
                }else{
                    $message = "Ce lien a expiré, veuillez demander un renvoi de changement de mot de passe.";
                }
            }else{
                $message = "Vos mots de passe ne correspondent pas.";
            }
        }else{
            $message = "Votre mot de passe doit contenir minimum une minuscule, une majuscule, un chiffre et un caractère spécial.";
        }
    }else{
        $message = "Veuillez renseigner tous les champs.";
    }
}

?>