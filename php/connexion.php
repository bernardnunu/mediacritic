<?php 

require("bdd.php");

require("menus.php");

// Connexion

if(isset($_POST['connect'])){
    $email = trim($_POST['email']);
    $mdp_crypted = hash('sha256', $_POST['mdp']);
    if(!empty($_POST['email']) AND !empty($_POST['mdp'])){
        $requser = $bdd->prepare("SELECT * FROM users WHERE email = ? AND motdepasse = ? AND validated = 1");
        $requser->execute(array($email, $mdp_crypted));
        $userinfo = $requser->fetch();
        if($userinfo == true){
            $userID = intval($userinfo['id']);

            date_default_timezone_set('Europe/Paris');
            $jour = date('Y-m-d H:i:s');
            $date = date('Y-m-d H:i:s', strtotime($jour. ' + 7 days'));

            $uuid_auth = uniqid();
            $ip_user = $_SERVER['REMOTE_ADDR'];

            $ip_user_hash = sha1($ip_user);
            $uuid_auth_hash = sha1($uuid_auth);

            $string = "".$userID."-".$ip_user_hash."-".$uuid_auth_hash."";
            $insert = $bdd->prepare("UPDATE users SET uuid_auth = ?, ip_user = ?, date_fin_session = ? WHERE id = ".$userID."");
            if(! $insert->execute(array($uuid_auth, $ip_user, $date))){
                print_r($insert->errorInfo());
            }else{
                setcookie('login', $string, time()+86400*7, '/', '', false, true);
                header("Location: /");
                exit();
            }

        }else{
            $message = "Ce compte n'existe pas ou n'est pas validé (email).";
        }
    }else{
        $message = "Veuillez renseigner tous les champs.";
    }
}

?>
