<?php

session_start();

require("bdd.php");

require("menus.php");

// Redirection

if($connected == false || $god == false){
    header("Location: /");
    exit();
}

if(isset($_POST['gestion'])){
    header("Location: /mc/page-auteur.php");
    exit();
}

// Ajout éléments

$listeAuteur = [];
$tmpArray = [];

function verifieChamp($nom){
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    verifieChamp('titre');
    verifieChamp('description');
    verifieChamp('datesortie');
    verifieChamp('duree');
    if (isset($_POST['ajouterAuteur']) || isset($_POST['supprimerAuteur']) || isset($_POST['rechercheAuteur'])){
        if(isset($_SESSION['listeAuteur'])){
            if(count($_SESSION['listeAuteur']) > 0){
                unset($_SESSION['listeAuteur']);
            }
        }
        if(isset($_FILES['upfile'])){
            unset($_FILES['upfile']);
        }

        if(isset($_POST['idAuteur'])){
            $nombreLigne = count($_POST['idAuteur']);
            for($i = 0; $i < $nombreLigne; $i++){
                if (! isset($_POST['supprimerAuteur']) || $_POST['supprimerAuteur'] != $_POST['idAuteur'][$i]){
                    array_push($listeAuteur, array(0 => $_POST['idAuteur'][$i], 1 => $_POST['nomAuteur'][$i]));
                }
            }
        }
        if(! isset($_POST['supprimerAuteur']) && ! isset($_POST['rechercheAuteur']) && ! empty($_POST['rechercheReal']) || ! empty($_POST['realisateur'])){
            $tmpArray = explode("|", $_POST['realisateur']);
            array_push($listeAuteur, array(0 => $tmpArray[0], 1 => $tmpArray[1]));
        }
        $_SESSION['listeAuteur'] = $listeAuteur;

    }else{
        if($connected == true && $god == true){
            if($_FILES['upfile']['size'] != 0){
                if(!empty($_POST['titre'])){
                    if(!preg_match("/[;*<>=()\[\]\|\/\$]/", $_POST['titre'])){
                        if(!preg_match("/[*<>=\[\]\|&\/\$]/", $_POST['description'])){
                            if(!preg_match("/[;*<>=()\[\]\|&\/\$]/", $_POST['duree'])){
                                if(! empty($_SESSION['listeAuteur'])){                                

                                    $errors= array();
                                    $file_name = $_FILES['upfile']['name'];
                                    $file_size = $_FILES['upfile']['size'];
                                    $file_tmp = $_FILES['upfile']['tmp_name'];
                                    $file_type = $_FILES['upfile']['type'];
                                    $explode = explode('.',$_FILES['upfile']['name']);
                                    $file_ext = strtolower(end($explode));
                                    $file_ext = pathinfo($_FILES['upfile']['name'], PATHINFO_EXTENSION);
                                    $extensions = array("jpeg","jpg","png");

                                    $titre = trim($_POST['titre']);
                                    $titreURL1 = strtolower(str_replace(' ', '-', $_POST['titre']));
                                    $titreURL = preg_replace("/[^a-zA-Z0-9-()+]/", '', $titreURL1);
                                    $categorie = trim($_POST['categorie']);
                                    $description = trim($_POST['description']);
                                    $datesortie = trim($_POST['datesortie']);
                                    $duree = trim($_POST['duree']);

                                    $description64 = base64_encode($description);

                                    if(in_array($file_ext,$extensions)=== false){
                                        $errors[]="L'extention de l'image n'est pas valide (PNG, JPG, JPEG).";
                                    }
                                        
                                    if($file_size > 2097152){
                                        $errors[]= "L'image ne doit pas dépasser 2MB.";
                                    }

                                    if($_POST['categorie'] == 1){
                                        $sousRep = "films";
                                    }elseif($_POST['categorie'] == 2){
                                        $sousRep = "series";
                                    }elseif($_POST['categorie'] == 3){
                                        $sousRep = "jv";
                                    }

                                    if(empty($errors)==true){
                                        $file_uniqueid = md5(uniqid() . $file_name . rand(0,100));
                                        $chemin_image = "../uploads/".$sousRep."/" . $file_uniqueid . ".$file_ext";
                                        $image = move_uploaded_file($file_tmp, $chemin_image);
                                        if($image == true){
                                            try{
                                                $bdd->beginTransaction();
                                                date_default_timezone_set('Europe/Paris');
                                                $date = date('Y-m-d H:i:s');
                                                $bddImage = $bdd->prepare("INSERT INTO images (image, dateAjout) VALUES (?, ?);");
                                                $bddItem = $bdd->prepare("INSERT INTO items (titre, titreURL, description, idcategorie, datesortie, duree) VALUES (?, ?, ?, ?, ?, ?);");
                                                $bddItemsImages = $bdd->prepare("INSERT INTO itemsimages (idItems, idImages, dateAjout) VALUES (?, ?, ?);");
                                                $bddImage->execute(array(preg_replace('/^\.\./', '', $chemin_image), $date));
                                                $lastIdImage = $bdd->lastInsertId();
                                                $bddItem->execute(array($titre, $titreURL, $description64, $categorie, $datesortie, $duree));
                                                $lastIdItem = $bdd->lastInsertId();
                                                $bddItemsImages->execute(array($lastIdItem, $lastIdImage, $date));
                                                $idItemsImages = $bdd->lastInsertId();
                                                $bddLink = 'INSERT INTO linkrealisateur (idRealisateur, idItems) VALUES';
                                                foreach($_SESSION['listeAuteur'] as $auteurItem){
                                                    $bddLink = $bddLink . " (" . $auteurItem[0] . ", " . $lastIdItem . "),";
                                                }
                                                $bddLink = substr($bddLink, 0, -1).";";
                                                $bddLinkRealisateur = $bdd->prepare($bddLink);
                                                $bddLinkRealisateur->execute();
                                                if(! $bdd->commit()){
                                                    print_r($bdd->errorInfo());
                                                }else{
                                                    session_destroy();
                                                    header("Location: /$sousRep/$titreURL/".$lastIdItem."");
                                                    exit();
                                                }
                                            }catch( PDOExecption $e ){
                                                $bdd->rollback();
                                                $message = "Erreur: " . $e->getMessage();
                                            }
                                        }else{
                                            $message = "Erreur lors de l'upload de l'image.";
                                        }
                                    }else{
                                        $message = "Une erreur s'est produite.";
                                        print_r($errors);
                                    }
                                }else{
                                    $message = "Veuillez saisir au moins un auteur.";
                                }
                            }else{
                                $message = "Mauvaise syntaxe pour le champs 'Durée'.";
                            }
                        }else{
                            $message = "Mauvaise syntaxe pour le champs 'Description'.";
                        }
                    }else{
                        $message = "Mauvaise syntaxe pour le champs 'Titre'.";
                    }
                }else{
                    $message = "Veuillez renseigner tous les champs.";
                }
            }else{
                $message = "Veuillez insérer une image.";
            }
        }else{
            header("Location: /");
            exit();
        }
    }
}

?>