<?php

require("bdd.php");

require("menus.php");

// Obtention de l'uuid

$_SESSION['uuid'] = $_GET['uuid'];

// Vérification et validation

if(isset($_POST['valider'])){
    $link = $bdd->prepare("SELECT * FROM users WHERE uuid_validation = ? AND validated = 0");
    $link->execute(array($_SESSION['uuid']));
    $goodlink = $link->fetch();
    if($goodlink == true){
        $validated = $bdd->prepare("UPDATE users SET validated = 1 WHERE uuid_validation = ?");
        if(!$validated->execute(array($_SESSION['uuid']))){
            print_r($validated->errorInfo());
        }else{

            $headers = array(
                'From' => 'webmaster',
                'Reply-To' => 'webmaster@mediacritic.fr',
                'X-Mailer' => 'PHP/' . phpversion()
            );

            $envoiemail = mail($goodlink['email'], "Merci pour votre inscription !", "Bonjour !\n\nMerci d'avoir validé votre compte, vous pouvez désormais vous connecter sur mediacritic.fr en toute sécurité !\n\nÀ bientôt !\n\nNOTE : Ceci est un mail provisoire (design), il sera modifié d'ici quelques semaines.", $headers);
            if($envoiemail == true){
                $message1 = "Votre compte a bien été validé, vous pouvez désormais vous connecter.";
            }
        }
    }else{
        $message = "Ce lien a expiré ou votre compte a déjà été validé.";
    }
}

?>