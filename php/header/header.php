<?php

// if(isset($_POST['cookie'])){
//     $auth = explode("-", $_COOKIE['login']);
//     $updateCookie = $bdd->prepare("UPDATE users SET cookie = 1 WHERE id = $auth[0]");
//     if($updateCookie->execute()){
//         header("Location: /");
//     }
// }

?>

<link rel="stylesheet" type="text/css" href="../../css/header.css">

<header id="header">

<nav class="navbar navbar-expand-xl navbar-dark bg-secondary">
    <a id="titleMain" class="navbar-brand yellow text-warning" href="/">MediaCritic <i class="fas fa-star"></i></a>
    <button class="navbar-toggler bg-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon text-white"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <?php if($connected == true && $god == true){ echo'<a class="nav-link text-warning" href="/mc/page-atelier.php">Atelier <i class="fas fa-hammer"></i></a>';} ?>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="/">Populaire <i class="fas fa-chart-line"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="/mc/page-recents.php">Récents <i class="fas fa-clock"></i></a>
            </li>
            <li class="nav-item">
                <?php if($connected == true){ echo '<a class="nav-link text-white" href="/mc/page-profil.php">Profil <i class="fas fa-user-circle"></i></a>';} ?>
            </li>
            <li class="nav-item">
                <?php if($connected == false){ echo '<a class="nav-link text-warning" href="/mc/page-connexion.php">Connexion <i class="fas fa-mouse"></i></a>';} ?>
            </li>
            <li class="nav-item">
                <?php if($connected == false){ echo '<a class="nav-link text-warning" href="/mc/page-inscription.php">S\'inscrire <i class="fas fa-users"></i></a>';} ?>
            </li>
            <li class="nav-item">
                <?php if($connected == true){ echo '<a class="nav-link text-danger" href="/php/deconnexion.php"><i class="fas fa-power-off"></i></a>';} ?>
            </li>
            <input type="checkbox" id="switch" name="theme"><label for="switch" id="toggle"></label>
        </ul>
        <form id="searchAll" class="form-inline my-xl-0" method="get" action="/php/recherche.php">
            <input id="searchBar" class="form-control" name="search" type="search" placeholder="Recherche dans...">
            <select id="select" name="categorie">
            <?php
                $sql2 = "SELECT * FROM categorie;";
                echo "<option value='all' selected>Tout</option>";
                foreach($bdd->query($sql2) as $categorie){
                    echo "<option value='".$categorie['idcategorie']."'>" . $categorie['categorie'] . "</option>";
                }
            ?>
            </select>
            <input id="loupe" class="btn btn-outline-success my-2 my-sm-0" name="loupe" type="submit" value="&#128269;">
        </form>
    </div>
</nav>

<?php

// if($connected == true){
//     if($userinfo['cookie'] == 0){
//         echo "<div id='cookie'>
//             <h3 id='titreCookie'>Nous respectons votre vie privée</h3>
//             <p id='texteCookie'>
//                 Nous utilisons des techologies telles que des cookies pour nous permettre de mieux comprendre comment le site est utilisé. En continuant à utiliser ce site, vous acceptez cette politique.
//                 Nos cookies nous sont utiles uniquement pour les connexions de nos utilisateurs et sont supprimés définitivement après une semaine d'inactivité sur le site.
//             </p>
//             <form action='' method='post'>
//                 <input id='compris' type='submit' name='cookie' value='Compris'>
//             </form>
//         </div>";
//     }
// }

?>

</header>

<script>

let checkbox = document.querySelector('input[name=theme]');

function applyTheme() {
    let dark = localStorage.getItem('dark');
    if (dark === 'true') {
        document.documentElement.setAttribute('data-theme', 'dark');
        checkbox.checked=true;
    } else {
        document.documentElement.setAttribute('data-theme', 'light');
        checkbox.checked=false;
    };
};

applyTheme();

checkbox.addEventListener('change', function() {
    localStorage.setItem('dark', this.checked);
    if(this.checked) {
        trans();
        document.documentElement.setAttribute('data-theme', 'dark');
    } else {
        trans();
        document.documentElement.setAttribute('data-theme', 'light');
    }
});

const trans = () => {
    document.documentElement.classList.add('transition');
    window.setTimeout(() => {
        document.documentElement.classList.remove('transition');
    }, 1000);
};

</script>