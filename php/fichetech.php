<?php

session_start();

require("bdd.php");

require("menus.php");

// Traitement pour la génération de la page

$_SESSION['iditem'] = $_GET['iditem'];

$sql = $bdd->prepare("SELECT items.iditems, items.titre, items.datesortie, items.description, items.duree, categorie.categorie, images.image FROM items 
INNER JOIN categorie ON items.idcategorie = categorie.idcategorie 
INNER JOIN itemsimages ON itemsimages.iditems = items.iditems 
INNER JOIN images ON images.id = itemsimages.idimages AND images.deleted = false 
WHERE items.iditems = ".$_SESSION['iditem']."");
$sql->execute();
$infoItem = $sql->fetch();
$sortie = date("d M Y", strtotime($infoItem['datesortie']));

// Traitement formulaire avis/commentaire

function verifieChamp($nom){
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

if($connected == true){
    if(isset($_POST['avis'])){
        verifieChamp('zonetexte');
        if(!empty($_POST['zonetexte'])){
            if(!preg_match("/[;*<>=)(\[\]\|&\/\$]/", $_POST['zonetexte'])){
                if(strlen($_POST['zonetexte']) < 200){

                    $zonetexte = base64_encode(trim($_POST['zonetexte']));
                    $already = $bdd->prepare("SELECT * FROM avis WHERE iditems = ? AND idusers = ?");
                    $already->execute(array($_SESSION['iditem'], $userID));
                    $check = $already->fetch();

                    if($check == false){

                        try{
                            $bddAvis = $bdd->prepare("INSERT INTO avis (avis, dateCreation, iditems, idusers, dateModification) VALUES (?, ?, ?, ?, ?);");
                            try{
                                $bdd->beginTransaction();
                                date_default_timezone_set('Europe/Paris');
                                $date = date('Y-m-d H:i:s');
                                $bddAvis->execute(array($zonetexte, $date, $_SESSION['iditem'], $userID, $date));
                                if(! $bdd->commit()){
                                    print_r($bdd->errorInfo());
                                }else{
                                    $message2 = "Votre avis a bien été posté.";
                                }
                            }catch( PDOExecption $e ){
                                $bdd->rollback();
                                $message = "Erreur: " . $e->getMessage();
                            }
                        }catch( PDOExecption $e ){
                            $message = "Erreur: " . $e->getMessage();
                        }

                    }else{
                        $message = "Vous avez déjà donné votre avis sur ".$infoItem['titre'].".";
                    }
                }else{
                    $message = "Votre message ne doit pas dépasser 200 caractères.";
                }
            }else{
                $message = "Votre avis contient des caractère(s) interdit(s). ";
            }
        }else{
            $message = "Veuillez renseigner un avis.";
        }
    }    
}else{
    $message = "Veuillez vous connecter pour laisser un avis.";
}

// Modifier avis

if(isset($_POST['modify'])){
    if($connected == true){

        $info = $bdd->prepare("SELECT * FROM avis WHERE
        dateModification = ( SELECT MAX(dateModification) FROM avis WHERE idusers = ? AND iditems = ?);");
        $info->execute(array($userID, $_SESSION['iditem']));
        $yesornot = $info->fetch();

        if($yesornot == true){
            header("Location: /mc/page-modification.php?idavis=".$yesornot['idavis']."");
            exit();
        }
    }
}

?>