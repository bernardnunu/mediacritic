<?php

session_start();

require("bdd.php");

require("menus.php");

// Filtre variable

if(isset($_POST['film'])){
    $_SESSION['texte'] = "films";
    $_SESSION['req'] = "AND items.idcategorie = 1";
    header("Location: ".$_SERVER['PHP_SELF']."");
}
if(isset($_POST['serie'])){
    $_SESSION['texte'] = "séries";
    $_SESSION['req'] = "AND items.idcategorie = 2";
    header("Location: ".$_SERVER['PHP_SELF']."");
}
if(isset($_POST['jv'])){
    $_SESSION['texte'] = "jeux vidéos";
    $_SESSION['req'] = "AND items.idcategorie = 3";
    header("Location: ".$_SERVER['PHP_SELF']."");
}
if(isset($_POST['all']) || ! isset($_SESSION['req'])){
    $_SESSION['texte'] = "élements";
    $_SESSION['req'] = "";
    header("Location: ".$_SERVER['PHP_SELF']."");
}

// Pagination

$itemsParPage = 20;

if(isset($_GET['page']) AND $_GET['page'] > 0){
    $_GET['page'] = intval($_GET['page']);
    $pageCourante = $_GET['page'];
}else{
    $pageCourante = 1;
}

$depart = ($pageCourante-1)*($itemsParPage);

?>
