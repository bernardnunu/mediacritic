<?php

session_start();

require("bdd.php");

require("menus.php");

// Redirection

if($connected == false || $god == false){
    header("Location: /");
    exit();
}

if(isset($_POST['gestion'])){
    header("Location: /mc/page-auteur.php");
    exit();
}

// Données pour affichage

$_SESSION['iditem'] = $_GET['iditem'];
$sql = $bdd->prepare("SELECT items.iditems, items.idcategorie, items.titre, items.datesortie, items.description, categorie.categorie, items.duree FROM items 
INNER JOIN categorie ON items.idcategorie = categorie.idcategorie 
INNER JOIN realisateur INNER JOIN linkrealisateur ON realisateur.idrealisateur = linkrealisateur.idRealisateur AND items.iditems = ".$_SESSION['iditem']."",
array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false));
$sql->execute(array());
$infoItem = $sql->fetch();

// Modification

$listeAuteur = [];
$tmpArray = [];

function verifieChamp($nom){
    if(isset($_POST[$nom])){
        $_SESSION[$nom] = $_POST[$nom];
    }
}

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    verifieChamp('newtitre');
    verifieChamp('newdescription');
    verifieChamp('newdatesortie');
    verifieChamp("newduree");
    if (isset($_POST['ajouterAuteur']) || isset($_POST['supprimerAuteur']) || isset($_POST['rechercheAuteur'])){
        if(isset($_SESSION['listeAuteur'])){
            if(count($_SESSION['listeAuteur']) > 0){
                unset($_SESSION['listeAuteur']);
            }
        }
        if(isset($_FILES['upfile'])){
            unset($_FILES['upfile']);
        }

        if(isset($_POST['idAuteur'])){
            $nombreLigne = count($_POST['idAuteur']);
            for($i = 0; $i < $nombreLigne; $i++){
                if (! isset($_POST['supprimerAuteur']) || $_POST['supprimerAuteur'] != $_POST['idAuteur'][$i]){
                    array_push($listeAuteur, array(0 => $_POST['idAuteur'][$i], 1 => $_POST['nomAuteur'][$i]));
                }
            }
        }
        if(! isset($_POST['supprimerAuteur']) && ! isset($_POST['rechercheAuteur']) && ! empty($_POST['rechercheReal']) || ! empty($_POST['newrealisateur'])){
            $tmpArray = explode("|", $_POST['newrealisateur']);
            array_push($listeAuteur, array(0 => $tmpArray[0], 1 => $tmpArray[1]));
        }
        $_SESSION['listeAuteur'] = $listeAuteur;

    }else{

        if($connected == true && $god == true){
            if(!empty($_POST['newtitre'])){
                if(!preg_match("/[;*<>=()\[\]\|\/\$]/", $_POST['newtitre'])){
                    if(!preg_match("/[*<>=\[\]\|&\/\$]/", $_POST['newdescription'])){
                        if(!preg_match("/[;*<>=()\[\]\|&\/\$]/", $_POST['newduree'])){
                            if(! empty($_SESSION['listeAuteur'])){

                                $titre = trim($_POST['newtitre']);
                                $titreURL1 = strtolower(str_replace(' ', '-', $_POST['newtitre']));
                                $titreURL = preg_replace("/[^a-zA-Z0-9-()+]/", '', $titreURL1);
                                $categorie = trim($_POST['newcategorie']);
                                $description = trim($_POST['newdescription']);
                                $datesortie = trim($_POST['newdatesortie']);
                                $duree = trim($_POST['newduree']);
                                if(empty($_POST['newdatesortie'])){
                                    $datesortie = NULL;
                                }

                                $description64 = base64_encode($description);

                                if($_POST['newcategorie'] == 1){
                                    $sousRep = "films";
                                }elseif($_POST['newcategorie'] == 2){
                                    $sousRep = "series";
                                }elseif($_POST['newcategorie'] == 3){
                                    $sousRep = "jv";
                                }
                                
                                if($_FILES['newupfile']['size'] != 0){

                                    $errors= array();
                                    $file_name = $_FILES['newupfile']['name'];
                                    $file_size = $_FILES['newupfile']['size'];
                                    $file_tmp = $_FILES['newupfile']['tmp_name'];
                                    $file_type = $_FILES['newupfile']['type'];
                                    $explode = explode('.',$_FILES['newupfile']['name']);
                                    $file_ext = strtolower(end($explode));
                                    $file_ext = pathinfo($_FILES['newupfile']['name'], PATHINFO_EXTENSION);
                                    $extensions = array("jpeg","jpg","png");

                                    if(in_array($file_ext,$extensions)=== false){
                                        $errors[]="L'extention de l'image n'est pas valide (PNG, JPG, JPEG).";
                                    }
                                        
                                    if($file_size > 2097152){
                                        $errors[]= "L'image ne doit pas dépasser 2MB.";
                                    }

                                    if(empty($errors)==true){
                                        $file_uniqueid = md5(uniqid() . $file_name . rand(0,100));
                                        $chemin_image = "../uploads/".$sousRep."/" . $file_uniqueid . ".$file_ext";
                                        $image = move_uploaded_file($file_tmp,$chemin_image);
                                            if($image == true){
                                                try{
                                                $genericIDImages = [1, 49485];
                                                $bdd->beginTransaction();
                                                date_default_timezone_set('Europe/Paris');
                                                $date = date('Y-m-d H:i:s');
                                                $bddGetImage = $bdd->prepare("SELECT * FROM itemsimages II INNER JOIN images I ON II.idImages = I.id WHERE idItems = ? AND I.deleted = false ORDER BY I.dateAjout DESC LIMIT 1;");
                                                $bddModifyImage = $bdd->prepare("UPDATE images SET DeletedDate = ?, deleted = ? WHERE id = ?");
                                                $bddDeleteImage = $bdd->prepare("DELETE FROM itemsimages WHERE idItems = ? AND idImages = ?");
                                                $bddImage = $bdd->prepare("INSERT INTO images (image, dateAjout) VALUES (?, ?);");
                                                $bddItem = $bdd->prepare("UPDATE items SET titre = ?, titreURL = ?, description = ?, idcategorie = ?, datesortie = ?, duree = ? WHERE iditems = ?;");
                                                $bddItemsImages = $bdd->prepare("INSERT INTO itemsimages (idItems, idImages, dateAjout) VALUES (?, ?, ?);");
                                                //Récuperer l'ID de l'image en cours
                                                $bddGetImage->execute(array($_SESSION['iditem']));
                                                $requeteImage = $bddGetImage->fetch();
                                                $idImage = intval($requeteImage['idImages']);
                                                // Modifier le champs deleted et deleted_date de l'image en cours
                                                if(! in_array($idImage, $genericIDImages)){
                                                    $bddModifyImage->execute(array($date, 1, $idImage));
                                                }else{
                                                    $bddDeleteImage->execute(array($_SESSION['iditem'], $idImage));
                                                }
                                                // Ajouter l'enregistremant dans la table images
                                                $bddImage->execute(array(preg_replace('/^\.\./', '', $chemin_image), $date));
                                                $lastIdImage = $bdd->lastInsertId();
                                                // Ajouter l'enregistrement dans la table ItemsImages
                                                $bddItemsImages->execute(array($_SESSION['iditem'], $lastIdImage, $date));
                                                // Ajouter l'enregistrement dans la table Items
                                                $bddItem->execute(array($titre, $titreURL, $description64, $categorie, $datesortie, $duree, $_SESSION['iditem']));
                                                $delete = $bdd->prepare("DELETE FROM linkrealisateur WHERE idItems = ".$_SESSION['iditem']."");
                                                $delete->execute();
                                                $bddLink = 'INSERT INTO linkrealisateur (idRealisateur, idItems) VALUES';
                                                foreach($_SESSION['listeAuteur'] as $auteurItem){
                                                    $bddLink = $bddLink . " (" . $auteurItem[0] . ", " . $_SESSION['iditem'] . "),";
                                                }
                                                $bddLink = substr($bddLink, 0, -1).";";
                                                $bddLinkRealisateur = $bdd->prepare($bddLink);
                                                $bddLinkRealisateur->execute();
                                                    if(! $bdd->commit()){
                                                        print_r($bdd->errorInfo());
                                                    }else{
                                                        session_destroy();
                                                        header("Location: /$sousRep/$titreURL/".$infoItem['iditems']."");
                                                        exit();
                                                    }
                                                }catch( PDOExecption $e ){
                                                    $bdd->rollback();
                                                    $message = "Erreur: " . $e->getMessage();
                                                }
                                            }else{
                                                $message = "Erreur lors de l'upload de l'image.";
                                            }
                                    }else{
                                        $message = "L'extention est invalide ou l'image est rop lourde.";
                                    }

                                }else{
                                    $req = $bdd->prepare("UPDATE items SET titre = ?, titreURL = ?, description = ?, idcategorie = ?, datesortie = ?, duree = ? WHERE iditems = ?;");
                                    $delete = $bdd->prepare("DELETE FROM linkrealisateur WHERE idItems = ".$_SESSION['iditem']."");
                                    $delete->execute();
                                    $bddLink = 'INSERT INTO linkrealisateur (idRealisateur, idItems) VALUES';
                                        foreach($_SESSION['listeAuteur'] as $auteurItem){
                                            $bddLink = $bddLink . " (" . $auteurItem[0] . ", " . $_SESSION['iditem'] . "),";
                                        }
                                    $bddLink = substr($bddLink, 0, -1).";";
                                    $bddLinkRealisateur = $bdd->prepare($bddLink);
                                    $bddLinkRealisateur->execute();
                                        if(! $req->execute(array($titre, $titreURL, $description64, $categorie, $datesortie, $duree, $_SESSION['iditem']))){
                                            print_r($req->errorInfo());
                                        }else{
                                            session_destroy();
                                            header("Location: /$sousRep/$titreURL/".$_SESSION['iditem']."");
                                            exit();
                                        }
                                }
                            }else{
                                $message = "Veuillez saisir au moins un auteur.";
                            }
                        }else{
                            $message = "Mauvaise syntaxe pour le champs 'Durée'.";
                        }
                    }else{
                        $message = "Mauvaise syntaxe pour le champs 'Description'.";
                    }
                }else{
                    $message = "Mauvaise syntaxe pour le champs 'Titre'.";
                }
            }else{
                $message = "Veuillez renseigner tous les champs.";
            }
        }else{
            header("Location: /");
            exit();
        }
    }
}else{
    $nbLigne = 0;
    $reqTable = "SELECT * FROM linkrealisateur INNER JOIN realisateur ON linkrealisateur.idRealisateur = realisateur.idrealisateur WHERE linkrealisateur.idItems = " . $infoItem['iditems'] . "";
    foreach($bdd->query($reqTable) as $table){
        array_push($listeAuteur, array(0 => $table['idrealisateur'], 1 => $table['realisateur']));
    }
}
$_SESSION['listeAuteur'] = $listeAuteur;

?>