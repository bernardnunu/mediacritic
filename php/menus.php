<?php

// Connecté ?

if(isset($_COOKIE['login'])){
    $auth = explode('-', $_COOKIE['login']);
    $requser = $bdd->prepare("SELECT * FROM users WHERE id = ? AND date_fin_session >= NOW() AND validated = 1");
    $requser->execute(array($auth[0]));
    $userinfo = $requser->fetch();
    if($userinfo == true){
        $userID = intval($userinfo['id']);
        if($userID != 0){

            $ip_user_hash = sha1($userinfo['ip_user']);
            $uuid_auth_hash = sha1($userinfo['uuid_auth']);

            $ipActuelle = sha1($_SERVER['REMOTE_ADDR']);

            $cookieValide = "".$userID."-".$ip_user_hash."-".$uuid_auth_hash."";
            $cookieInconnu = "".$auth[0]."-".$ipActuelle."-".$auth[2]."";

            if($cookieInconnu == $cookieValide){
                $connected = true;
            }else{
                $connected = false;
                setcookie('login', '', '', '/');
                header("Location: /mc/page-connexion.php");
                exit();
            }
        }else {
            $connected = false;
        }
    }else{
        $connected = false;
        setcookie('login', '', '', '/');
        $null = $bdd->prepare("UPDATE users SET uuid_auth = NULL, ip_user = NULL, date_fin_session = NULL WHERE id = ".$userinfo['id']."");
        $null->execute();
        header("Location: /mc/page-connexion.php");
        exit();
    }
}else{
    $connected = false;
}

// Menu atelier

if($connected == true){
    $auth = explode('-', $_COOKIE['login']);
    $admin = $bdd->prepare("SELECT id FROM users WHERE id = ? AND admin = 1");
    $admin->execute(array($auth[0]));
    $adminInfo = $admin->fetch();

    if($adminInfo == true){
        $god = true;
    }else{
        $god = false;
    }
}else{
    $god = false;
}

?>