<?php

require("bdd.php");

require("menus.php");

$note = 0.00;

if(isset($_POST['1'])){
    $note = 5;
}elseif (isset($_POST['2'])){
    $note = 4;
}elseif (isset($_POST['3'])){
    $note = 3;
}elseif (isset($_POST['4'])){
    $note = 2;
}elseif (isset($_POST['5'])){
    $note = 1;
}else{
    echo "ERROR";
}

if($connected == true){
    if(isset($_POST['itemid'])){
        $itemid = intval($_POST['itemid']);
        $req = $bdd->prepare("INSERT INTO rates(iditems, idusers, rates) VALUES (?, ?, ?);");
        if($req->execute(array($itemid, $userID, $note))){
            header("Location: ".$_SERVER['HTTP_REFERER']."");
            exit();
        }else{
            print_r($req->errorInfo());
        }
    }else{
        echo '404';
    }
}else{
    header("Location: /");
    exit();
}

?>