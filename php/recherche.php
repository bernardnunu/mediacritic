<?php

require("bdd.php");

if(isset($_GET['loupe']) AND !empty(trim($_GET['search']))){
    if(strlen(trim($_GET['search'])) >= 2){
        if(! preg_match("/[\"{};*<>=)(\[\]\|\/\$]/", trim($_GET['search']))){
            $search = addslashes(trim($_GET['search']));
            header("Location: /mc/page-resultat.php?search=".$search."&filtre=".$_GET['categorie']."");
            exit();
        }else{
            header("Location: /");
            exit();
        }
    }else{
        header("Location: /");
        exit();
    }
}else{
    header("Location: /");
    exit();
}

?>
