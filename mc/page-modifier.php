<?php require("../php/modifier.php"); ?>

<!DOCTYPE html>
<html lang="fr-FR" data-theme="light">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/page-formulaire2.css">
    <link rel="stylesheet" type="text/css" href="../css/globaux.css">
    <title>MediaCritic - Modifications items</title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155163165-1"></script>
    <script src="/js/google.js"></script>
</head>
<body>

<div id="grid">

    <?php require("../php/header/header.php"); ?>

    <main id="main">

        <div id="block">

            <h1 class="titre">Modifier <?php echo $infoItem['titre']; ?> ?</h1>
            <form action="" class="form" method="post" enctype="multipart/form-data">
                <input type="text" class="case" name="newtitre" placeholder="Titre..." value="<?php if(isset($_SESSION['newtitre'])){echo $_SESSION['newtitre'];}else{echo $infoItem['titre'];} ?>">
                <input type="search" class="case" name="rechercheReal" placeholder="Rechercher un auteur...">
                <input type="submit" name="rechercheAuteur" value="Rechercher..." class="submit">
                <select class="select" name="newrealisateur">
                <?php
                if(isset($_POST['rechercheAuteur'])){
                    if(! empty(trim($_POST['rechercheReal']))){
                        $sql = $bdd->query("SELECT * FROM realisateur 
                        WHERE realisateur LIKE '%".$_POST['rechercheReal']."%' 
                        ORDER BY realisateur ASC");
                        foreach ($sql as $realisateur){
                            echo "<option value='" . $realisateur['idrealisateur'] . "|" . $realisateur['realisateur'] . "'>" . $realisateur['realisateur'] . "</option>";
                        }
                    }else{
                        $message = "Veuillez renseigner un auteur avant de rechercher.";
                    }
                }
                ?>
                </select>
                <input type="submit" class="submit" name='ajouterAuteur' value="Ajouter">
                <table class="listeAuteur" name="listeAuteur">
                    <tr>
                        <th>ID</th>
                        <th>Auteur</th>
                        <th>Supprimer</th>
                    </tr>
                    <?php
                    $nbligne = 0;
                    if(isset($_SESSION['listeAuteur'])){
                        $nbligne = count($_SESSION['listeAuteur']);
                        for($j = 0; $j < $nbligne; ++$j){
                            echo '<tr>
                            <td><input type="hidden" name="idAuteur[]" value="' . $_SESSION['listeAuteur'][$j][0] .'">' . $_SESSION['listeAuteur'][$j][0] . '</td>
                            <td><input type="hidden" name="nomAuteur[]" value="' . $_SESSION['listeAuteur'][$j][1] . '">' . $_SESSION['listeAuteur'][$j][1] . '</td>
                            <td>
                                <input type="submit" class="delete" name="supprimerAuteur" value="' . $_SESSION['listeAuteur'][$j][0] . '">
                            </td>
                            </tr>';
                        }
                    }
                    ?>
                </table>
                <input type="submit" name="gestion" value="Gestion auteurs" class="submit">
                <select class="select" name="newcategorie">
                <?php
                    $sql2 = "SELECT * FROM categorie;";
                    echo "<option value='" . $infoItem['idcategorie'] . "' selected>" . $infoItem['categorie'] . "</option>";
                    echo "<option>--------</option>";
                    foreach($bdd->query($sql2) as $categorie){
                        echo "<option value='" . $categorie['idcategorie'] . "'>" . $categorie['categorie'] . "</option>";
                    }
                ?>
                </select>
                <textarea name="newdescription" class="textarea" placeholder="Description..."><?php if(isset($_SESSION['newdescription'])){echo $_SESSION['newdescription'];}else{echo base64_decode($infoItem['description']);} ?></textarea>
                <input type="date" class="date" name="newdatesortie" value="<?php if(isset($_SESSION['newdatesortie'])){echo $_SESSION['newdatesortie'];}else{echo $infoItem['datesortie'];} ?>">
                <input type="text" class="case" name="newduree" placeholder="Durée..." value="<?php if(isset($_SESSION['newduree'])){echo $_SESSION['newduree'];}else{echo $infoItem['duree'];} ?>">
                <input type="file" id="file" class="select" name="newupfile">
                <input type="submit" class="submit" name="modifier" value="Modifier">
            </form>
            <?php
                if(isset($message)){
                    echo "<div id='message'>$message</div>";
                }
            ?>
            
        </div>

    </main>

    <?php require("../php/footer/footer.php"); ?>

</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
</html>