<?php require("../php/fichetech.php"); ?>

<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" data-theme="light">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="MediaCritic, page descriptive : <?php echo "(".$infoItem['categorie'].") ".$infoItem['titre'].""; ?>.">
    <meta property="og:title" content="<?php echo "(".$infoItem['categorie'].") ".$infoItem['titre'].""; ?> - MediaCritic">
    <meta property="og:description" content="MediaCritic, page descriptive : <?php echo "(".$infoItem['categorie'].") ".$infoItem['titre']."";; ?>.">
    <meta property="og:url" content="https://www.mediacritic.fr/mc/page-fiche.php?iditem=<?php echo $_SESSION['iditem']; ?>">
    <meta property="og:locale" content="fr_FR">
    <meta property="og:image" content="https://www.mediacritic.fr/favicon.ico">
    <meta property="og:type" content="website">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/page-fiche.css">
    <link rel="stylesheet" type="text/css" href="/css/globaux.css">
    <title><?php echo "(".$infoItem['categorie'].") ".$infoItem['titre'].""; ?> - MediaCritic</title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155163165-1"></script>
    <script src="/js/google.js"></script>
</head>
<body>

<div id="grid">

    <?php require("../php/header/header.php"); ?>

    <main id="main">

        <div id="infoge">

        <?php

            $req = $bdd->prepare("SELECT idrates FROM rates WHERE iditems = ?;");
            $req->execute(array($infoItem['iditems']));
            $reqtrue = $req->fetch();
            if($reqtrue == true){
                $nbNotes = 0;
                $totalNotes = 0;
                $req = $bdd->prepare("SELECT rates FROM rates WHERE iditems = ?;");
                $req->execute(array($infoItem['iditems']));
                $nbNotes = $req->rowCount();
                if($nbNotes != 1){$s = "s";}else{$s = "";}
                foreach($req as $note){
                    $totalNotes = $totalNotes + intval($note['rates']);
                }
                $moyenne = $totalNotes / $nbNotes;
                $moyennearrondie = number_format($moyenne,2);
            }else{
                $moyennearrondie = '&#8709';
                $nbNotes = 0;
                $s = "";
            }

            if(! empty($infoItem['duree'])){ $dureeItem = "<span>(" . $infoItem['duree'] . ")</span>"; }else{ $dureeItem = ""; }

            echo "<div id='partieinfo'>";
            if($connected == true && $god == true){echo "<a id='modifier' href='/mc/page-modifier.php?iditem=".$infoItem['iditems']."'><input type='submit' value='Modifier'></a>";}
            echo "<img id='image' src='" . $infoItem['image'] . "' alt='Image de ".str_replace("'", " ", $infoItem['titre'])."'>";
            echo "<h1 id='titre'>" . $infoItem['titre'] . "<br>$dureeItem</h1>";
            $real = "SELECT realisateur FROM linkrealisateur 
            INNER JOIN realisateur ON linkrealisateur.idRealisateur = realisateur.idrealisateur 
            WHERE idItems = ".$infoItem['iditems']."";
                foreach($bdd->query($real) as $realisateur){
                    echo "<h2 id='realisateur'>" . $realisateur['realisateur'] . "</h2>";
                }
            if(isset($infoItem['datesortie'])){echo "<p id='datesortie'>Sortie le " . $sortie . "</p>";}else{echo "<p id='datesortie'>Date de sortie non renseignée</p>";}
            if($connected == true){
                $req = $bdd->prepare("SELECT idrates FROM rates 
                WHERE idusers = ? AND iditems = ?;");
                $req->execute(array($userID, $infoItem['iditems']));
                $reqtrue = $req->fetch();
                if($reqtrue == false){
                echo ' <form class="stars" action="/php/traitement.php" method="post">
                        <input name="itemid" type="hidden" value="' . $infoItem['iditems'] . '">';
                echo '  <input type="submit" class="star" name="1" value="">
                        <input type="submit" class="star" name="2" value="">
                        <input type="submit" class="star" name="3" value="">
                        <input type="submit" class="star" name="4" value="">
                        <input type="submit" class="star" name="5" value="">
                    </form> ';
                    echo "<div class='infos'>
                        <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                        </div>";
                }else{
                    $knowrate = $bdd->prepare("SELECT rates FROM rates WHERE idusers = ".$userID." AND iditems=".$infoItem['iditems']."");
                    $knowrate->execute();
                    $infoRate = $knowrate->fetch();
                    echo "<div class='infos'>
                            <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                            <p class='ok'>Votre note : ".intval($infoRate['rates'])."/5</p>
                        </div>";
                }
            }else{
                echo "<p class='noconnect' style='margin-bottom: 10px'>Connectez-vous <br>pour noter</p>";
                echo "<div class='infos'>
                        <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                        </div>";
            }
            echo "</div>";
        ?>

            <div id="description">
                
            <?php
                echo "<p id='categorie'>" . $infoItem['categorie'] . "</p>";
                echo "<p id='syno'>Synopsis et détails <i class='fas fa-info-circle'></i></p>";
                echo "<p id='separate'>--------------------------------------</p>";
                if(! empty($infoItem['description'])){echo "<p id='texte'>" . base64_decode($infoItem['description']) . "</p>";}else{echo "<p id='texte'>Synopsis non renseigné.</p>";}
            ?>

            </div>

        </div>

        <div id="partiedeux">

            <div id="avis">

                <?php

                if($connected == true){
                    if(isset($_SESSION['zonetexte'])){
                        $texte = $_SESSION['zonetexte'];
                    }else{
                        $texte = "";
                    }
                    echo "<div id='poster'>";
                    echo "<form action='' method='post'>";
                    echo    "<textarea id='commentaire' name='zonetexte' placeholder='Avis sur ". str_replace("'", " ", $infoItem['titre']) . " ?'>".$texte."</textarea>";
                    echo    "<input type='submit' name='avis' id='submit' value='Poster'>";
                    echo "</form>
                    </div>";
                    if(isset($message)){
                        echo "<div class='message'><p id='message'>$message</p></div>";
                    }elseif(isset($message2)){
                        echo "<div class='message'><p id='message2'>$message2</p></div>";
                    }
                }else{
                    echo "<div id='blocknoconnect'><p class='noconnect'>Vous devez vous <br>connecter pour laisser un avis.</p></div>";
                }

                ?>
                
            </div>

            <div id="espcom">

                <?php
                
                $sqlcom = "SELECT idavis, avis, dateCreation, iditems, last_idusers, last_dateModification, pseudo, email, motdepasse, admin, idImages, image FROM 
                ( SELECT idusers AS last_idusers , MAX(dateModification) AS last_dateModification FROM avis A 
                WHERE A.iditems = ".$_SESSION['iditem']." AND A.idusers IN 
                ( SELECT idusers FROM avis WHERE iditems = '".$_SESSION['iditem']."' GROUP BY idusers ) GROUP BY A.idusers ) AS most_recent_avis 
                INNER JOIN avis B ON B.idusers = most_recent_avis.last_idusers AND B.dateModification = most_recent_avis.last_dateModification 
                INNER JOIN users U ON B.idusers = U.id LEFT JOIN ( usersimages UI INNER JOIN images I ON I.id = UI.idimages AND I.deleted = 'false' ) ON UI.idusers = U.id 
                ORDER BY B.dateModification DESC;";
                foreach ($bdd->query($sqlcom) as $commentaire){
                    echo "<div class='blockcom'>";
                    if($commentaire['admin'] == 1){echo "<p class='pseudo'><i class='fas fa-star text-warning'></i> De " . $commentaire['pseudo'] . " <img width='30px' height='30px;' src='" . $commentaire['image'] . "' alt='' class='rounded-circle'></p>";}
                    else{echo "<p class='pseudo'>De " . $commentaire['pseudo'] . " <img width='30px' height='30px;' src='" . $commentaire['image'] . "' alt='' class='rounded-circle'></p>";}
                    echo "<p class='dateCreation'>Le " . date("d M Y à H:i:s", strtotime($commentaire['dateCreation'])) . "</p>";
                    if($connected == true){if($commentaire['dateCreation'] != $commentaire['last_dateModification'] && $commentaire['last_idusers'] == $userID){echo "<p class='modif'>(Modifié)</p>";}}
                    echo "<p class='textecom'>" . base64_decode($commentaire['avis']) ."</p>";
                    if($connected == true){if($commentaire['last_idusers'] == $userID){echo "<form method='post' class='divmodify'><input type='submit' class='modify' name='modify' value='Modifier'></form>";}};
                    echo "</div>";
                }
                
                ?>

            </div>

        </div>
        
    </main>

    <?php require("../php/footer/footer.php"); ?>

</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
</html>