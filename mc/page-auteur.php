<?php require("../php/auteur.php"); ?>

<!DOCTYPE html>
<html lang="fr-FR" data-theme="light">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/page-formulaire2.css">
    <link rel="stylesheet" type="text/css" href="../css/globaux.css">
    <title>MediaCritic - Gestion auteurs</title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155163165-1"></script>
    <script src="/js/google.js"></script>
</head>
<body>

<div id="grid">

    <?php require("../php/header/header.php"); ?>

    <main id="main">

        <div id="block">

            <h1 class="titre">Gestion auteurs</h1>
            <form action="" class="form" method="post" enctype="multipart/form-data">
                <input type="text" class="case" name="ajouterAuteur" placeholder="Nouvel auteur...">
                <input type="search" class="case" name="rechercheReal" placeholder="Rechercher un auteur...">
                <input type="submit" name="rechercheAuteur" value="Rechercher..." class="submit">
                <select class="select" name="realisateur">
                <?php
                if(isset($_POST['rechercheAuteur'])){
                    if(! empty(trim($_POST['rechercheReal']))){
                        $sql = "SELECT * FROM realisateur 
                        WHERE realisateur LIKE '%".$_POST['rechercheReal']."%' 
                        ORDER BY realisateur ASC";
                        foreach ($bdd->query($sql) as $realisateur){
                            echo "<option value='" . $realisateur['idrealisateur'] . "|" . $realisateur['realisateur'] . "'>" . $realisateur['realisateur'] . "</option>";
                        }
                    }else{
                        $message = "Veuillez renseigner un auteur avant de rechercher.";
                    }
                }
                ?>
                </select>
                <input type="text" class="case" name="modifierAuteur" placeholder="Modifier auteur...">
                <input type="submit" class="submit" name="soumettre" value="Ajouter/Modifier">
            </form>
            <?php
                if(isset($message)){
                    echo "<div id='message'>$message</div>";
                }elseif(isset($message1)){
                    echo "<div id='message1'>$message1</div>";
                }
            ?>
            
        </div>

    </main>

    <?php require("../php/footer/footer.php"); ?>

</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
</html>