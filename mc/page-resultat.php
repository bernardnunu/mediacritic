<?php require("../php/main.php"); ?>
<?php 

    if(! isset($_GET['filtre']) || ! isset($_GET['search']) || empty($_GET['filtre']) || empty($_GET['search'])){
        header("Location: /");
        exit();
    }

?>

<!DOCTYPE html>
<html lang="fr-FR" data-theme="light">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/css/globaux.css">
    <title>MediaCritic - Résultat(s) de <?php echo $_GET['search']; ?></title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155163165-1"></script>
    <script src="/js/google.js"></script>
</head>
<body>

<div id="grid">

    <?php require("../php/header/header.php"); ?>

    <main id="main">

        <?php 

            if($_GET['filtre'] == 1){
                $filtre = "AND items.idcategorie = 1";
                $name = "films";
            }
            if($_GET['filtre'] == 2){
                $filtre = "AND items.idcategorie = 2";
                $name = "séries";
            }
            if($_GET['filtre'] == 3){
                $filtre = "AND items.idcategorie = 3";
                $name = "jeux vidéos";
            }
            if($_GET['filtre'] === "all"){
                $filtre = "";
                $name = "éléments";
            }

            $keywords = trim($_GET['search']);
            $affichage = $bdd->prepare("SELECT DISTINCT items.iditems, items.titre, items.titreURL, categorie.categorie, categorie.idcategorie, images.image FROM items 
            INNER JOIN categorie ON items.idcategorie = categorie.idcategorie ".$filtre."
            INNER JOIN itemsimages ON itemsimages.iditems = items.iditems 
            INNER JOIN images ON images.id = itemsimages.idimages AND images.deleted = false
            INNER JOIN linkrealisateur ON items.iditems = linkrealisateur.idItems 
            INNER JOIN realisateur ON linkrealisateur.idRealisateur = realisateur.idrealisateur
            WHERE (items.titre LIKE '%".trim($keywords)."%' OR items.titre LIKE '%".trim(str_replace('-', ' ', $keywords))."%' OR items.titre LIKE '%".trim(str_replace(' ', '-', $keywords))."%') OR realisateur.realisateur LIKE '%".$keywords."%' ORDER BY items.titre ASC;");
            $affichage->execute();
            $count = $affichage->rowCount();
            if($count > 1){
                $s = 's';
            }else{
                $s = '';
            }

        ?>

        <div id="partieun">
            <div class="alert">
                <p class="messagealert">
                    <?php echo "Résultat{$s} de"; ?><?php echo '"'.stripslashes($_GET['search']).'"'; ?> dans <span><?php echo $name; ?>.</span><br>
                    <?php echo "{$count} résultat{$s}" ?>
                </p>
            </div>
        </div>

        <div id="items">

            <?php
            
                if($count > 0){
                    if($count < 150){
                        $iditems = array();
                        foreach($affichage as $result){
                            $nbNotes = 0;
                            $totalNotes = 0;
                            $req = $bdd->prepare("SELECT rates FROM rates 
                            WHERE iditems = ?;");
                            $req->execute(array($result['iditems']));
                            $nbNotes = $req->rowCount();
                            if($nbNotes > 1){$s = "s";}else{$s = "";}
                            foreach ($req as $note) {
                                $totalNotes = $totalNotes + intval($note['rates']);
                            }
                            if($nbNotes === 0){
                                $moyennearrondie = '&#8709;';
                            }else{
                                $moyenne = $totalNotes / $nbNotes;  
                                $moyennearrondie = number_format($moyenne,2);
                            }
                            
                            if(! in_array($result['iditems'],$iditems)){
                                echo "<div class='block'>";
                                echo "<p class='category'>";  
                                if(isset($_SESSION['tabId'])){if(in_array($result['iditems'], $_SESSION['tabId'])){echo "<img class='top' src='/favicon.ico' alt='Image d une étoile' title='Top ".strtolower($result['categorie'])." du moment'>";}} 
                                echo "<span>".$result['categorie']."</span>";
                                echo "</p>";
                                if($connected == true && $god == true){echo "<a class='modifier' href='page-modifier.php?iditem=".$result['iditems']."'><input type='submit' value='Modifier'></a>";}
                                if($result['idcategorie'] == 1){echo "<a class='image' href='/films/".$result['titreURL']."/".$result['iditems']."'><img src='" . $result['image'] . "' alt='Image de ".str_replace("'", " ", $result['titre'])."'></a>";}
                                if($result['idcategorie'] == 2){echo "<a class='image' href='/series/".$result['titreURL']."/".$result['iditems']."'><img src='" . $result['image'] . "' alt='Image de ".str_replace("'", " ", $result['titre'])."'></a>";}
                                if($result['idcategorie'] == 3){echo "<a class='image' href='/jv/".$result['titreURL']."/".$result['iditems']."'><img src='" . $result['image'] . "' alt='Image de ".str_replace("'", " ", $result['titre'])."'></a>";}                                
                                echo "<h1 class='title_item'>" . $result['titre'] . "</h1>";
                                $real = $bdd->prepare("SELECT realisateur.realisateur FROM linkrealisateur 
                                INNER JOIN realisateur ON linkrealisateur.idRealisateur = realisateur.idrealisateur 
                                WHERE linkrealisateur.idItems = ".$result['iditems'].";");
                                $real->execute();
                                $nRows = $real->rowCount();
                                $listeReal = $real->fetch();
                                if($nRows > 1){
                                    echo "<h3 class='realisateur'>" . $listeReal['realisateur'] . " ...</h3>";
                                }else{
                                    echo "<h3 class='realisateur'>" . $listeReal['realisateur'] . "</h3>";
                                }
                                if($connected == true){
                                    $req = $bdd->prepare("SELECT idrates FROM rates 
                                    WHERE idusers = ? AND iditems = ?;");
                                    $req->execute(array($userID, $result['iditems']));
                                    $reqtrue = $req->fetch();
                                    if($reqtrue == false){
                                    echo ' <form class="stars" action="../php/traitement.php" method="post">
                                            <input name="itemid" type="hidden" value="' . $result['iditems'] . '">';
                                    echo '  <input type="submit" class="star" name="1" value="">
                                            <input type="submit" class="star" name="2" value="">
                                            <input type="submit" class="star" name="3" value="">
                                            <input type="submit" class="star" name="4" value="">
                                            <input type="submit" class="star" name="5" value="">
                                        </form> ';
                                        echo "<div class='infos'>
                                            <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                                            </div>";
                                    }else{
                                        $knowrate = $bdd->prepare("SELECT rates FROM rates WHERE idusers = ".$userID." AND iditems=".$result['iditems']."");
                                        $knowrate->execute();
                                        $infoRate = $knowrate->fetch();
                                        echo "<div class='infos'>
                                                <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                                                <p class='ok'>Votre note : ".intval($infoRate['rates'])."/5</p>
                                            </div>";
                                    }
                                }else{
                                    echo "<p class='noconnect'>Vous devez vous <br>connecter pour noter.</p>";
                                    echo "<div class='infos'>
                                            <h2 class='moyenneetnb'>$moyennearrondie/5 ($nbNotes note".$s.")</h2>
                                            </div>";
                                }
                                echo "</div>";
                                array_push($iditems,$result['iditems']);
                            }
                        }
                    }else{
                        echo "<p id='noresult'>Les réponses sont trop nombreuses. Veuillez être plus précis dans votre recherche...</p>";
                    }
                }else{
                    echo '<p id="noresult">Aucun résultat pour "'.stripslashes($keywords).'"...</p>';
                }
                
            ?>

        </div>

    </main>

    <?php require("../php/footer/footer.php"); ?>

</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
</html>