<?php require("../php/connexion.php"); ?>

<!DOCTYPE html>
<html lang="fr-FR" prefix="og: http://ogp.me/ns#" data-theme="dark">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="Connectez-vous facilement pour profiter du catalogue complet de MediaCritic.">
    <meta property="og:title" content="MediaCritic - Connexion">
    <meta property="og:description" content="Connectez-vous facilement pour profiter du catalogue complet de MediaCritic.">
    <meta property="og:url" content="https://www.mediacritic.fr/mc/page-connexion.php">
    <meta property="og:locale" content="fr_FR">
    <meta property="og:image" content="https://www.mediacritic.fr/favicon.ico">
    <meta property="og:type" content="website">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/page-formulaire.css">
    <link rel="stylesheet" type="text/css" href="../css/globaux.css">
    <title>MediaCritic - Connexion</title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155163165-1"></script>
    <script src="/js/google.js"></script>
</head>
<body>
    
<div id="grid">

    <?php require("../php/header/header.php"); ?>

    <main id="main">

        <div id="block"><h1 id="title">Connexion</h1></div>

        <form action="" method="post" id="form">
            <input class="case" type="email" name="email" placeholder="Email...">
            <input class="case" type="password" name="mdp" placeholder="Mot de passe...">
            <a href="page-inscription.php" class="option">Vous n'avez pas de compte ?</a>
            <a href="page-demande.php" class="option">Mot de passe oublié ?</a>
            <input type="submit" name="connect" id="submit" value="Se connecter">
            <?php
            if(isset($message)){
            echo "<div id='message'>$message</div>";
            }
            ?>
        </form>
        
    </main>

    <?php require("../php/footer/footer.php"); ?>

</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
</html>