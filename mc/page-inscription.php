<?php require("../php/register.php"); ?>

<!DOCTYPE html>
<html lang="fr-FR" data-theme="light">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="Inscrivez-vous gratuitement et facilement pour profiter du catalogue complet de MediaCritic.">
    <meta property="og:title" content="MediaCritic - Inscription">
    <meta property="og:description" content="Inscrivez-vous gratuitement et facilement pour profiter du catalogue complet de MediaCritic.">
    <meta property="og:url" content="https://www.mediacritic.fr/mc/page-inscription.php">
    <meta property="og:locale" content="fr_FR">
    <meta property="og:image" content="https://www.mediacritic.fr/favicon.ico">
    <meta property="og:type" content="website">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/page-formulaire.css">
    <link rel="stylesheet" type="text/css" href="../css/globaux.css">
    <title>MediaCritic - Inscription</title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155163165-1"></script>
    <script src="/js/google.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
    
<div id="grid">

    <?php require("../php/header/header.php"); ?>

    <main id="main">

        <div id="block"><h1 id="title">Inscription</h1></div>
    
        <form action="" method="post" id="form">
            <input class="case" type="text" name="pseudo" placeholder="Pseudo..." value="<?php if(isset($_SESSION['pseudo'])){echo $_SESSION['pseudo'];} ?>">
            <p class="tips">Pseudo entre 4 et 20 caractères.</p>
            <input class="case" type="email" name="email" placeholder="Email..." value="<?php if(isset($_SESSION['email'])){echo $_SESSION['email'];} ?>">
            <p class="tips">Saisir un email valide.</p>
            <input class="case" type="email" name="email2" placeholder="Confirmez email..." value="<?php if(isset($_SESSION['email2'])){echo $_SESSION['email2'];} ?>">
            <input class="case" type="password" name="mdp" placeholder="Mot de passe...">
            <p class="tips">Mot de passe entre 8 et 20 caractères.</p>
            <input class="case" type="password" name="mdp2" placeholder="Confirmez mot de passe...">
            <a href="page-connexion.php" class="option">Déjà un compte ?</a>
            <div id="captcha" class="g-recaptcha" data-sitekey="6LeCWdsUAAAAAG_GJmZOKbCScZyjpyKXLtSHbKOb" data-callback="enableBtn"></div>
            <input type="submit" name="inscription" id="submit" value="S'inscrire" disabled>
            <!-- <p class="tips">Les inscriptions sont temporairement désactivées.</p> -->

            <script>
                function enableBtn(){
                    document.getElementById("submit").disabled = false;
                }
            </script>

            <?php
            if(isset($message)){
            echo "<div id='message'>$message</div>";
            }
            ?>
        </form>

    </main>

    <?php require("../php/footer/footer.php"); ?>

</div>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/29a822e896.js" crossorigin="anonymous"></script>
</html>