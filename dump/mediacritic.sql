-- MySQL dump 10.17  Distrib 10.3.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: site
-- ------------------------------------------------------
-- Server version	10.3.18-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `avis`
--

DROP TABLE IF EXISTS `avis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avis` (
  `idavis` int(11) NOT NULL AUTO_INCREMENT,
  `avis` text NOT NULL,
  `dateCreation` datetime NOT NULL,
  `iditems` int(11) DEFAULT NULL,
  `idusers` int(11) DEFAULT NULL,
  `dateModification` datetime NOT NULL,
  PRIMARY KEY (`idavis`),
  KEY `idusers` (`idusers`),
  KEY `iditems` (`iditems`),
  CONSTRAINT `avis_ibfk_1` FOREIGN KEY (`idusers`) REFERENCES `users` (`id`),
  CONSTRAINT `avis_ibfk_2` FOREIGN KEY (`iditems`) REFERENCES `items` (`iditems`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avis`
--

LOCK TABLES `avis` WRITE;
/*!40000 ALTER TABLE `avis` DISABLE KEYS */;
/*!40000 ALTER TABLE `avis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie` (
  `idcategorie` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(20) NOT NULL,
  PRIMARY KEY (`idcategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `dateAjout` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `DeletedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `iditems` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) NOT NULL,
  `titreURL` varchar(150) NOT NULL,
  `description` text DEFAULT NULL,
  `idcategorie` int(11) NOT NULL,
  `datesortie` date DEFAULT NULL,
  `duree` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`iditems`),
  KEY `items_ibfk_1` (`idcategorie`),
  CONSTRAINT `items_ibfk_1` FOREIGN KEY (`idcategorie`) REFERENCES `categorie` (`idcategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemsimages`
--

DROP TABLE IF EXISTS `itemsimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemsimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idItems` int(11) NOT NULL,
  `idImages` int(11) NOT NULL,
  `dateAjout` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idItems` (`idItems`),
  KEY `idImages` (`idImages`),
  CONSTRAINT `itemsimages_ibfk_1` FOREIGN KEY (`idItems`) REFERENCES `items` (`iditems`),
  CONSTRAINT `itemsimages_ibfk_2` FOREIGN KEY (`idImages`) REFERENCES `images` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemsimages`
--

LOCK TABLES `itemsimages` WRITE;
/*!40000 ALTER TABLE `itemsimages` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemsimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linkrealisateur`
--

DROP TABLE IF EXISTS `linkrealisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linkrealisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idItems` int(11) NOT NULL,
  `idRealisateur` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_iditems_idrealisateur` (`idItems`,`idRealisateur`),
  KEY `idItems` (`idItems`),
  KEY `idRealisateur` (`idRealisateur`),
  CONSTRAINT `linkrealisateur_ibfk_1` FOREIGN KEY (`idItems`) REFERENCES `items` (`iditems`),
  CONSTRAINT `linkrealisateur_ibfk_2` FOREIGN KEY (`idRealisateur`) REFERENCES `realisateur` (`idrealisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linkrealisateur`
--

LOCK TABLES `linkrealisateur` WRITE;
/*!40000 ALTER TABLE `linkrealisateur` DISABLE KEYS */;
/*!40000 ALTER TABLE `linkrealisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rates` (
  `idrates` int(11) NOT NULL AUTO_INCREMENT,
  `rates` float(9,2) NOT NULL,
  `idusers` int(11) NOT NULL,
  `iditems` int(11) NOT NULL,
  PRIMARY KEY (`idrates`),
  UNIQUE KEY `unique_index` (`idusers`,`iditems`),
  KEY `iditems` (`iditems`),
  CONSTRAINT `rates_ibfk_1` FOREIGN KEY (`idusers`) REFERENCES `users` (`id`),
  CONSTRAINT `rates_ibfk_2` FOREIGN KEY (`iditems`) REFERENCES `items` (`iditems`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rates`
--

LOCK TABLES `rates` WRITE;
/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realisateur`
--

DROP TABLE IF EXISTS `realisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realisateur` (
  `idrealisateur` int(11) NOT NULL AUTO_INCREMENT,
  `realisateur` varchar(50) NOT NULL,
  PRIMARY KEY (`idrealisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realisateur`
--

LOCK TABLES `realisateur` WRITE;
/*!40000 ALTER TABLE `realisateur` DISABLE KEYS */;
/*!40000 ALTER TABLE `realisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `motdepasse` varchar(80) NOT NULL,
  `admin` tinyint(1) DEFAULT 0,
  `uuid_validation` varchar(50) NOT NULL,
  `validated` tinyint(1) DEFAULT 0,
  `uuid_renew` varchar(50) DEFAULT NULL,
  `renewed` tinyint(1) DEFAULT 0,
  `uuid_auth` varchar(50) DEFAULT NULL,
  `ip_user` varchar(50) DEFAULT NULL,
  `date_fin_session` datetime DEFAULT NULL,
  `cookie` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersimages`
--

DROP TABLE IF EXISTS `usersimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUsers` int(11) NOT NULL,
  `idImages` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idUsers` (`idUsers`),
  KEY `idImages` (`idImages`),
  CONSTRAINT `usersimages_ibfk_1` FOREIGN KEY (`idUsers`) REFERENCES `users` (`id`),
  CONSTRAINT `usersimages_ibfk_2` FOREIGN KEY (`idImages`) REFERENCES `images` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersimages`
--

LOCK TABLES `usersimages` WRITE;
/*!40000 ALTER TABLE `usersimages` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersimages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-21 20:01:14
